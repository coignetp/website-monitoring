from windows.Window import Window
from fetch.Website import Website

import curses


class WindowWebsites(Window):
    """
    This contains the list of websites that are observed. The user can select one
    of them.
    The menu can be scrolled if the number of websites is too big to fit in the window
        - websites: the list of websites
        - actualChoice: the index of the selected website
        - firstDisplayChoice: the first choice to display on the window. Used to
          scroll the menu to see all the websites
    """

    def __init__(self, screen, window, title: str, colors: dict, websites: list):
        Window.__init__(self, screen, window, title, colors)
        self.websites = websites
        self.actualChoice = 0
        self.firstDisplayedChoice = 0

    def display(self, screen, colors: dict):
        """
        Displays all the choices that can fit in the window. The selected website
        is highlighted
        """
        self.window.erase()

        lastDisplayedChoice = min(
            len(self.websites),
            self.firstDisplayedChoice + self.window.getmaxyx()[0] - 2,
        )

        for choice in range(self.firstDisplayedChoice, lastDisplayedChoice):
            displayPositionY = choice + 1 - self.firstDisplayedChoice
            self.websites[choice].display_as_choice(
                self.window,
                displayPositionY,
                1,
                colors,
                highlighted=(choice == self.actualChoice),
            )

        Window.display(self, screen, colors)

    def choice_down(self):
        """
        Changes the selected website. Selects the one just after the actual one
        if their is one. Else do nothing.
        Updates the firstDisplayedChoice to have a good scrolling.
        """
        self.actualChoice = min(len(self.websites) - 1, self.actualChoice + 1)

        linesAvailable = self.window.getmaxyx()[0] - 3
        if self.firstDisplayedChoice + linesAvailable < self.actualChoice:
            self.firstDisplayedChoice = self.actualChoice - linesAvailable

    def choice_up(self):
        """
        Changes the selected website. Selects the one just before the actual one
        if their is one. Else do nothing.
        Updates the firstDisplayedChoice to have a good scrolling.
        """
        self.actualChoice = max(0, self.actualChoice - 1)
        if self.firstDisplayedChoice > self.actualChoice:
            self.firstDisplayedChoice = self.actualChoice
