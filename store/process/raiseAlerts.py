from DatabaseInterface import DatabaseInterface


def raise_alerts(alerts: list, databaseInterface: DatabaseInterface) -> bool:
    """
    Takes the alerts list and converts it into a correct influx query.
    Returns False if the list is empty
    """

    if len(alerts) == 0:
        return False

    listOfAlerts = [
        {
            "measurement": "alert",
            "tags": {"url": alert["url"]},
            "fields": {
                "availability": alert["availability"],
                "available": alert["available"],
            },
        }
        for alert in alerts
    ]

    # Databse writing
    databaseInterface.write(listOfAlerts)

    return True

