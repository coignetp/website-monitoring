from fetch.Website import Website
from windows.WindowAlerts import WindowAlerts
from utils import print_verbose

import threading
import json
import requests


class UpdaterManager:
    """
    The updaters will call periodically some store routes to update
    statistics and alerts
    """

    def __init__(self, websites: list):
        """
        Initiates the list manager with the list of observed websites.
        """
        self.updaters = {}

        self.websites = websites
        self.windowAlerts = None

    def delete(self):
        """
        At the end of the client script, this will end all
        the updaters.
        """
        for key in self.updaters.keys():
            self.updaters[key].cancel()
            print_verbose(f"Deleting {key} timer")

    def start(self, windowAlerts: WindowAlerts):
        """
        Starts all the updaters that will get the new statistics and alerts
            - windowAlerts: the window to callback when new alerts are found
        """
        self.windowAlerts = windowAlerts

        getLastStatRoute = "http://127.0.0.1:5000/get_last_stat"
        self.statistics_updater(getLastStatRoute, 10, 10 * 60)
        self.statistics_updater(getLastStatRoute, 60, 60 * 60)

        getNewAlertsRoute = "http://127.0.0.1:5000/get_new_alerts"
        self.alerts_updater(getNewAlertsRoute, 5)

    def statistics_updater(
        self, getLastStatRoute: str, refreshTime: int, windowLength: int
    ):
        """
        Gets the new statistics of windowLenght sec
            - getLastStatsRoute: the route to call to get the new statistics
            - refreshTime: the interval between two call of the route in seconds
            - windowLength: the length of the statistic window in seconds
        """
        try:
            for website in self.websites:
                website.update_statistics(getLastStatRoute, windowLength)

        except Exception as e:
            print(f"Statistics {windowLength} couldn't be updated")
            print(e)
        finally:
            # Prepares next update
            self.updaters[f"stats_{refreshTime}s"] = threading.Timer(
                refreshTime,
                lambda: self.statistics_updater(
                    getLastStatRoute, refreshTime, windowLength
                ),
            )
            self.updaters[f"stats_{refreshTime}s"].start()

    def alerts_updater(self, getNewAlertsRoute: str, refreshTime: int):
        """
        Get the new alerts and update the websites.
            - getNewAlertsRoute: the route to get the new alerts
            - refreshTime: the interval between two call of the route in seconds
        """
        try:
            alertsRequest = requests.get(
                f"{getNewAlertsRoute}/{refreshTime * 1000}", timeout=3
            )

            alerts = json.loads(alertsRequest.text)

            self.windowAlerts.add_alerts(alerts, self.websites)
        except Exception as e:
            print("Alerts couldn't be updated")
        finally:
            # This method will be called again in refreshTime - 0.5s.
            # The -0.5s is here to prevent the client from missing an alert.
            # Indeed, the query to the database doesn't take into account the
            # time to make the request.
            # If an alert is selected twice, it will be filtered by add_alerts method
            self.updaters[f"alerts_{refreshTime}s"] = threading.Timer(
                refreshTime - 0.5,
                lambda: self.alerts_updater(getNewAlertsRoute, refreshTime),
            )
            self.updaters[f"alerts_{refreshTime}s"].start()
