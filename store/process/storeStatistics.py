from DatabaseInterface import DatabaseInterface


def store_statistics(
    statisticsToWrite: dict, windowLength: int, databaseInterface: DatabaseInterface
) -> bool:
    """
    Takes the statisticsToWrite dict and converts it into a correct influx query.
    windowLength is in seconds and is used to name the measurement correctly.
    Returns False if the dict is empty
    """
    if not statisticsToWrite:
        return False

    listOfStatistics = [
        {
            "measurement": f"statistic_{windowLength}s",
            "tags": {"url": key},
            "fields": statisticsToWrite[key],
        }
        for key in statisticsToWrite.keys()
    ]

    # Databse writing
    databaseInterface.write(listOfStatistics)

    return True

