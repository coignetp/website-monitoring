from Website import Website
from utils import print_verbose

import json
import time


class Observer:
    """
    The observer class has a list of websites, and it periodically makes
    requests to them. Then their metrics are stored.
    """

    def __init__(self, configFileName: str):
        """
        Configures the observer according to the configuration file
            - configFileName: the name of the config.json
        """
        configFile = open(configFileName)

        config = json.load(configFile)

        # The interval between two poll of the same website in s
        self.pollInterval = float(config["global"]["poll_interval"])

        if not self.is_config_correct():
            print("Incorrect configuration file for global parameters")
            return

        # The list of websites to watch
        self.websites = []
        for websiteToObserve in config["websites"]:
            if self.is_url_correct(websiteToObserve["url"]):
                self.websites.append(Website(websiteToObserve["url"]))

    def run(self):
        """
        Infinite loop for the observer to always run. Checks once more if the 
        configuration is correct before begining the loop
        """

        if not self.is_config_correct():
            print("Incorrect configuration file for global parameters")
            return

        while True:
            beginTime = time.time()

            for website in self.websites:
                website.observe()

            elapsedTime = time.time() - beginTime

            # A poll should be done every self.pollInterval. The requests takes
            # some time, so the sleeping time is not self.pollInterval
            timeLeft = self.pollInterval - elapsedTime

            # Sleep at least 0.2s
            time.sleep(max(0.2, timeLeft))

    def is_config_correct(self) -> bool:
        """
        Checks if the parameters given in the configuration file are correct
        """
        return self.pollInterval > 0

    def is_url_correct(self, url: str) -> str:
        """
        Checks if an url is correct.
        Only checks if there is a http or https at the begining.
        """
        if url[:7] != "http://" and url[:8] != "https://":
            print(f"No http or https for the url {url}. Can't observe this website.")
            return False

        return True
