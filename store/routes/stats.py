import DatabaseInterface
from main import StoreApp
from flask import request, g
import json


def is_stats_request_conform(statRequest: dict) -> bool:
    """
    Checks if all the required field are here and if the window_length
    is correct.
    """
    if statRequest.keys() < {"url", "window_length"} or statRequest[
        "window_length"
    ] not in [2 * 60, 10 * 60, 60 * 60]:
        return False

    return True


@StoreApp.route("/get_last_stat", methods=["GET"])
def route_get_last_stat():
    """
    Route used by the client to get the last stats concerning.

    The request is made with a JSON which looks like this:

    {
        'url': 'https://www.google.com',
        'window_length': 600
    }

    The window_length is the windowLength of the wanted statistic in seconds
    """

    statRequest = request.get_json()

    # Check if the alerts can be inserted in the database
    if not is_stats_request_conform(statRequest):
        badStats = f"Bad statistic request {statRequest}"
        print(badStats)
        return badStats

    statistic = StoreApp.config["DBINTERFACE"].get_last_stat(
        statRequest["url"], statRequest["window_length"]
    )

    if statistic:
        # Convert the status_codes into dict.
        # They were stored as string because of influxDB
        statistic["last_status_codes"] = json.loads(statistic["last_status_codes"])

    return json.dumps(statistic)
