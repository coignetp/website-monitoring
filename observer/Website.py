import time
import requests

from utils import print_verbose
from Metric import Metric


class Website:
    """
    Each website object contains the global information about the website to observe
    """

    def __init__(self, url: str, urlStoreMetric="http://127.0.0.1:5000/metrics"):
        """
        Initiates a website to observe.
            - url: the url of the website
            - urlStoreMetric: the route to send metrics to.
        """
        # The name of the website to observe
        self.url = url

        self.urlStoreMetric = urlStoreMetric

        print_verbose(f"Website {url} will be observe")

    def observe(self):
        """
        Makes a request to have updated information about the website
        """
        # The timestamp at the begining of the request
        requestTimestamp = time.time()
        req = requests.get(self.url, timeout=2)

        m = Metric(self.url, req.status_code, req.elapsed, requestTimestamp)
        m.send(self.urlStoreMetric)
