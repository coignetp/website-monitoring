from DatabaseInterface import DatabaseInterface


def add_availability_to_dict(
    statisticsToWrite: dict, windowLength: int, databaseInterface: DatabaseInterface
):
    """
    Adds the availability with all the status codes of the observed websites to the 
    statisticsToWrite dict.
    windowLength is in seconds and is used for the Influx query to get the last metrics.
    """
    statusCodeResult = databaseInterface.query(
        f"SELECT COUNT(*) FROM metric WHERE time > now() - {windowLength}s GROUP BY url, status_code"
    )

    okStatusCodes = {url: 0 for url in statisticsToWrite.keys()}
    totalStatusCodes = {url: 0 for url in statisticsToWrite.keys()}

    # Looping on every couple (url, status_code)
    for statusCodekey in statusCodeResult.keys():
        numberStatusCodes = 0
        url = statusCodekey[1]["url"]

        # This loop should iterate only once to get the result of the COUNT(*)
        # A loop is needed because statusCodeResult[statusCodekey] is a generator
        for elementStatusCode in statusCodeResult[statusCodekey]:
            numberStatusCodes += elementStatusCode["count_elapsed_time"]

        statusCodeName = statusCodekey[1]["status_code"]
        statisticsToWrite[url]["status_codes"][statusCodeName] = numberStatusCodes
        totalStatusCodes[url] += numberStatusCodes

        # Every status_code between 200 and 299 is considered as a success.
        if int(statusCodeName) >= 200 and int(statusCodeName) < 300:
            okStatusCodes[url] += numberStatusCodes

    for url in statisticsToWrite.keys():
        # Can happen at the begining when there is no metric
        if totalStatusCodes[url] != 0:
            availability = okStatusCodes[url] / totalStatusCodes[url]
            statisticsToWrite[url]["availability"] = availability

