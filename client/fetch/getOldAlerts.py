from windows.WindowAlerts import WindowAlerts

import json
import requests


def get_old_alerts(
    getNewAlertsRoute: str, websites: list, timeWindow: int, windowAlerts: WindowAlerts
):
    """
    Gets the alerts which are timeWindow seconds old. This is used when the client 
    starts to have the last alerts in memory.
        - getNewAlertsRoute: the route to have the new alerts
        - websites: the list of websites displayed
        - timeWindow: is in seconds
        - windowAlerts: the windowAlerts that will display the information
    """
    try:
        alertsRequest = requests.get(
            f"{getNewAlertsRoute}/{timeWindow * 1000}", timeout=3
        )

        alerts = json.loads(alertsRequest.text)

        windowAlerts.add_alerts(alerts, websites)

    except Exception as e:
        print("Couldn't get the old alerts")
        print(e)
