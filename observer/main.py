import utils
from Website import Website
from Observer import Observer

from time import sleep
import argparse


if __name__ == "__main__":
    # Gets arguments
    parser = argparse.ArgumentParser(
        description="Poll the websites for the website monitoring tool"
    )
    parser.add_argument(
        "-v",
        "--verbose",
        help="Activate the verbose mode of the observer",
        action="store_true",
    )
    args = parser.parse_args()

    # Activates (or not) the verbose mode
    utils.ARG_VERBOSE = args.verbose

    observer = Observer(utils.get_real_path("../config.json"))

    observer.run()
