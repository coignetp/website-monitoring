from windows.Window import Window
from utils import print_verbose

import curses
import json


class WindowAlerts(Window):
    """
    The part of the screen that will display the alerts.
        - alerts: all the previous alerts
    """

    def __init__(self, screen, window, title: str, colors: dict):
        """
        Initiates the class with calling __init__ parent
        """
        Window.__init__(self, screen, window, title, colors)
        self.alerts = []

    def display(self, screen, colors: dict):
        """
        Displays all the alerts to the WindowAlerts
            - screen: the screen of the dashboard
            - colors: the colors of the dashboard
        """
        self.window.erase()

        maxDisplayedAlert = min(len(self.alerts), self.window.getmaxyx()[0] - 2)
        for alertIndex in range(0, maxDisplayedAlert):
            self.display_alert(
                self.window, alertIndex + 1, self.alerts[-1 - alertIndex], colors
            )

        Window.display(self, screen, colors)

    def display_alert(self, window, y: int, alert: dict, colors: dict):
        """
        Displays an alert in the window alert
            - window: the window to display
            - y: the position where the alert should be displayed
            - alert: the concerned alert
            - colors: colors of the dashboard
        """
        self.window.addstr(
            y, 1, str(alert["time"])[:16], curses.color_pair(colors["title"])
        )
        if alert["available"]:
            self.window.addstr(y, 18, "[UP]", curses.color_pair(colors["good_news"]))
        else:
            self.window.addstr(y, 18, "[DOWN]", curses.color_pair(colors["bad_news"]))

        self.window.addstr(
            y,
            26,
            "{:.2f}".format(alert["availability"] * 100) + "% " + alert["url"],
            curses.color_pair(colors["text"]),
        )

    def add_alerts(self, alerts: list, websites: list):
        """
        Adds the new alerts to the window.
        Checks before if the alert wasn't already received
            - alerts: list of the new alerts
            - websites: list of the websites observed
        """
        for alert in alerts:
            for website in websites:
                if website.url == alert["url"]:
                    # If the alert doesn't give additional information
                    # Can happen if an alert is received twice
                    if website.available == alert["available"]:
                        print_verbose(f"Alert {alert} already received")
                    else:
                        website.set_availability(
                            alert["available"], alert["availability"]
                        )
                        self.alerts.append(alert)
