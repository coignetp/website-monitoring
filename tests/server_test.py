from flask import Flask

"""
This application is a fake server to test the alert logic.
It is faking server error with error 500 sometimes (enough 
error to raise an alert) .
It runs on the port 42042
"""


fakeApp = Flask(__name__)


@fakeApp.route("/")
def index():
    counter = fakeApp.config["COUNTER"] + 1
    fakeApp.config.update(COUNTER=counter)

    if counter <= 20:
        return "OK"
    if counter <= 30:
        return "empty", 500
    if counter >= 50:
        fakeApp.config.update(COUNTER=0)

    return "OK"


if __name__ == "__main__":
    fakeApp.config.update(COUNTER=0)

    fakeApp.run(port=42042)
