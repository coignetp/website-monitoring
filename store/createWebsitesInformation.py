import json


def create_websites_information(configFileName: str) -> (list, list):
    """
    Reads the config.json thanks to configFileName and returns a list of websites
    url to observe and their availability threshold.
    """
    websites = []
    websitesAvailabilityThreshold = []
    configFile = open(configFileName)

    # Reads only the websites config
    websitesConfig = json.load(configFile)["websites"]
    configFile.close()

    for websiteObserved in websitesConfig:
        websites.append(websiteObserved["url"])
        websitesAvailabilityThreshold.append(websiteObserved["availability_threshold"])

    return websites, websitesAvailabilityThreshold
