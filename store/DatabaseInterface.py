from influxdb import InfluxDBClient
import json
from utils import print_verbose
import datetime


class DatabaseInterface:
    """
    The database class has everything needed to speak
    with the influxdb databse running.
    """

    def __init__(self, configFileName: str):
        """
        Initializes the database interface with a configuration file.
        Reads the database information (host, port, name, retention policy wanted) and the 
        credentials (username, password) and saves it.
        """
        configFile = open(configFileName)

        # Reads only the database config
        databaseConfig = json.load(configFile)["database"]
        configFile.close()

        self.host = databaseConfig["host"]
        self.port = int(databaseConfig["port"])
        self.databaseName = databaseConfig["database_name"]
        self.measuresDuration = databaseConfig["measures_duration"]
        self.measuresRecplication = int(databaseConfig["measures_replication"])
        self.username = databaseConfig["username"]
        self.password = databaseConfig["password"]

        # Will be True when the database interface will be connected to
        # the database
        self.connected = False

        # The client will be use to speak with the database
        self.client = None

    def connect(self) -> bool:
        """
        This is connecting the store to the InfluxDB database and creating the used database.
        Returns True if it is a success
        """
        try:
            self.client = InfluxDBClient(
                self.host, self.port, self.username, self.password
            )

            self.connected = True

            self.init_database()

            self.client.switch_database(self.databaseName)

        except Exception as e:
            print(f"Impossible to contact InfluxDB at {self.host}:{self.port}.\n{e}")
            return False

        return True

    def init_database(self) -> bool:
        """
        Creates the database if it does not exist.
        Returns True in case of a succes.
        """
        try:
            # Search for the name of the database if it already exists
            databaseList = self.client.get_list_database()
            for db in databaseList:
                if db["name"] == self.databaseName:
                    print_verbose(f"Database {self.databaseName} already existed")
                    return True

            # If databaseName doesn't exist, it needs to be created according to the different
            # parameters given in the config file
            self.client.create_database(self.databaseName)
            self.client.create_retention_policy(
                "outdated_measures",
                self.measuresDuration,
                self.measuresRecplication,
                database=self.databaseName,
            )
            print_verbose(
                f"New database {self.databaseName} created with a {self.measuresDuration} "
                + f"measure duration and {self.measuresRecplication} replication"
            )

        except Exception as e:
            print(e)
            return False

        return True

    def drop_database(self):
        """
        Drops the database if the client is connected
        """
        if self.connected:
            self.client.drop_database(self.databaseName)
            print_verbose(f"Database {self.databaseName} dropped")
            self.connected = False
        else:
            print_verbose(
                "Database can't be drop because the interface is not connected"
            )

    def write(self, points: str) -> bool:
        """
        Inserts the points in the wanted database after verifying the connection.
        """
        if not self.connected:
            print(f"Can't write in {self.databaseName} if there is no connection")
            return False

        try:
            self.client.write_points(points)
            for point in points:
                measureType = point["measurement"]
                print_verbose(f"* Insertion of {measureType} measure")

            return True
        except Exception as e:
            print(f"Impossible to write {points}")
            print(e)

    def get_last_stat(self, url: str, windowLength: int) -> dict:
        """
        Returns the last statistic concerning the url with a window length
        of windowLength seconds.
        """
        if not self.connected:
            print(
                f"Can't get last stat in {self.databaseName} if there is no connection"
            )
            return {}

        # The {url} needs to be simple quoted to specify that url is a tag
        query = f"SELECT LAST(*) FROM statistic_{windowLength}s WHERE \"url\"='{url}' GROUP BY *"
        stat = list(self.client.query(query))

        if len(stat) == 0:
            print(f"Query {query} had no results")
            return {}

        # Gets the element as a dict
        stat = stat[0][0]

        return stat

    def get_new_alerts(self, elapsedMs: int) -> []:
        """
        Returns all the new alerts for elapsedMs ms.
        """
        if not self.connected:
            print(
                f"Can't get new alerts in {self.databaseName} if there is no connection"
            )
            return []

        query = f"SELECT * FROM alert WHERE time > now() - {elapsedMs}ms"

        alerts = list(self.client.query(query))

        if len(alerts) > 0:
            alerts = alerts[0]

        return alerts

    def query(self, query: str):
        """
        Makes the requested query after checking connection.
        Returns the ResultSet
        """
        if not self.connected:
            print(f"Can't query in {self.databaseName} if there is no connection")
            return None

        return self.client.query(query)
