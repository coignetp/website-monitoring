from DatabaseInterface import DatabaseInterface
from utils import print_verbose
from process.raiseAlerts import raise_alerts
from process.storeStatistics import store_statistics
from process.addAvailabilityToDict import add_availability_to_dict

import threading
import json


class UpdaterManager:
    """
    This class manages all the statistics and alert updaters.
    It calls the functions periodically to compute the statistic
    or the alert over a windowLength.
    Here there is:
        - statistic over 10min refreshed every 10 sec
        - statistic over 60min refreshed every 1 min
        - alert over 2min refreshed every 5 sec
    """

    def __init__(
        self,
        databaseInterface: DatabaseInterface,
        websites: list,
        websitesAvailibilityThreshold: list,
    ):
        """
        Starts all the updaters.
            - databaseInterface: will be used by the updaters to get the 
              metrics stored
            - websites: the list of websites observed
        """

        self.databaseInterface = databaseInterface
        self.websites = websites
        self.websitesAvailable = [True] * len(websites)
        self.websitesAvailibilityThreshold = websitesAvailibilityThreshold

        self.updaters = [None] * 3

        # Starts all the updaters
        self.statistics(0, 10, 10 * 60)
        self.statistics(1, 60, 60 * 60)
        self.alerts(2, 5, 2 * 60)

    def stop(self):
        """
        Stops all the updaters
        """
        for updater in self.updaters:
            updater.cancel()
            print_verbose("Deleting updater")

    def statistics(self, updaterIndex: int, refreshTime: int, windowLength: int):
        """
        The statistics updaters. Computes interesting values about the response time
        of each websites (mean, maximum, minimum, 10th percentile, 90th percentile) 
        every refreshTime seconds and using the metrics got during the last
        windowLength seconds.
        """
        # Prepares the next updates
        self.updaters[updaterIndex] = threading.Timer(
            refreshTime,
            lambda: self.statistics(updaterIndex, refreshTime, windowLength),
        )
        self.updaters[updaterIndex].start()

        # No statistic at the begining
        isStatEmpty = True

        metricsStatResult = self.databaseInterface.query(
            "SELECT MEAN(elapsed_time) as average_response_time,"
            "MAX(elapsed_time) as maximum_response_time,"
            "MIN(elapsed_time) as minimum_response_time,"
            "PERCENTILE(elapsed_time, 10) as percentile10_response_time,"
            "PERCENTILE(elapsed_time, 90) as percentile90_response_time "
            f"FROM metric WHERE time > now() - {windowLength}s GROUP BY url"
        )

        # Set the default dict where everything will be written
        statisticsToWrite = {}
        for url in self.websites:
            statisticsToWrite[url] = {"status_codes": {}}

        add_availability_to_dict(
            statisticsToWrite, windowLength, self.databaseInterface
        )

        for key in metricsStatResult.keys():
            # Gets the url in the key
            url = key[1]["url"]
            isStatEmpty = False

            # This loop should iterate only once to get the result of the query
            # A loop is needed because metricsStatResult[key] is a generator
            for websiteStatistic in metricsStatResult[key]:
                for stat in [
                    "average_response_time",
                    "maximum_response_time",
                    "minimum_response_time",
                    "percentile10_response_time",
                    "percentile90_response_time",
                ]:
                    # This can happen at the begining with the percentile when
                    # there is not a lot of metrics
                    if websiteStatistic[stat] == None:
                        statisticsToWrite[url][stat] = -1
                    else:
                        statisticsToWrite[url][stat] = int(websiteStatistic[stat])

                statisticsToWrite[url]["status_codes"] = json.dumps(
                    statisticsToWrite[url]["status_codes"]
                )

        # If there is some stat in the statisticsToWrite
        if not isStatEmpty:
            store_statistics(statisticsToWrite, windowLength, self.databaseInterface)

    def alerts(self, updaterIndex: int, refreshTime: int, windowLength: int):
        """
        The statistics updaters. Computes the availability of each website every
        refreshTime seconds using the metrics got during the last windowLength 
        seconds. If the availability is different from last time, then an alert
        is raised.
        """
        self.updaters[updaterIndex] = threading.Timer(
            refreshTime, lambda: self.alerts(updaterIndex, refreshTime, windowLength)
        )
        self.updaters[updaterIndex].start()

        # The alert that will be raised
        alerts = []

        # Sets the default dict where everything will be written
        statistics = {}
        for url in self.websites:
            statistics[url] = {"status_codes": {}}

        add_availability_to_dict(statistics, windowLength, self.databaseInterface)

        # Checks the new availability of every websites
        for i in range(len(self.websites)):
            url = self.websites[i]

            # At the begining there is not enough metrics to compute
            # the availability so statistics[url]["availability"] may not be defined
            if "availability" in statistics[url]:
                isAvailable = (
                    statistics[url]["availability"]
                    >= self.websitesAvailibilityThreshold[i]
                )

                if self.websitesAvailable[i] != isAvailable:
                    self.websitesAvailable[i] = isAvailable
                    alerts.append(
                        {
                            "url": url,
                            "availability": statistics[url]["availability"],
                            "available": isAvailable,
                        }
                    )

        if len(alerts) > 0:
            raise_alerts(alerts, self.databaseInterface)
