import requests
import time
import json
import datetime
from utils import print_verbose


class Metric:
    """
    Metric is a class that contains all information about a specific request.
    The metric is sent to the store
    """

    def __init__(
        self,
        url: str,
        statusCode: int,
        elapsedTime: datetime.timedelta,
        requestTimestamp: float,
    ):
        """
        Creates a new metric from a request to be sent later.
            - url: the url of the website observed
            - statusCode: the status code of the request made to the website
            - elapsedTime: the raw time of the request
            - requestTimestamp: the moment the request was sent
        """
        self.content = {
            "measurement": "metric",
            "tags": {
                "url": url,
                "status_code": statusCode,
                # Converts the float time to integer type in ms
                "request_timestamp": int(requestTimestamp * 1000),
            },
            "fields": {
                # Converts the timedelta into milliseconds. It is considered that the timedelta
                # is less than a day long because there would be a timeout before then
                "elapsed_time": elapsedTime.seconds * 1000
                + int(elapsedTime.microseconds / 1000)
            },
        }

    def send(self, destination: str):
        """
        Sends the metric to the destination
            - destination: the store route for metrics saving
        """
        try:
            req = requests.post(destination, json=[self.content], timeout=1)

            print_verbose(
                f'Metric ({self.content["tags"]["status_code"]}) sent: {req.status_code}'
            )

        except Exception as e:
            print("An error occured when trying to send a metric to the store")
            print(e)
