from windows.Window import Window
from fetch.Website import Website

import curses


class WindowStatistics(Window):
    """
    The window where the different statistics are displayed.
    """

    def __init__(self, screen, window, title: str, colors: dict, website: Website):
        """
        Initiates the class with calling the  __init__ parent.
            - website: the current selected website. Used to display the right
              statistics.
        """
        Window.__init__(self, screen, window, title, colors)

        self.website = website

    def display(self, screen, colors: dict):
        """
        Displays the statistics of the selected website to the screen.
        """
        self.window.erase()

        self.website.display_as_statistics(self.window, colors)

        Window.display(self, screen, colors)

    def set_website(self, website):
        """
        Updates the selected website so the right statistics are displayed.
        """
        self.website = website
