import curses


class Window:
    """
    Mother class of all the windows of the dashboard.
        - window: the curses window
        - title: the title of the window (displayed)
    """

    def __init__(self, screen, window, title: str, colors: dict):
        """
        Initiates the class and setting its background color
        """
        self.window = window
        self.title = title

        # The background color also affects the box color
        self.window.bkgd(" ", curses.color_pair(colors["screen_background"]))

    def display(self, screen, colors: dict):
        """
        Displays the border and the title of the window.
        Also refresh the window to display.
        Should be called at the end of the child display method
        """
        self.window.box()
        self.window.addstr(
            0,
            self.window.getmaxyx()[1] // 6,
            self.title,
            curses.color_pair(colors["title"]),
        )

        self.window.refresh()
