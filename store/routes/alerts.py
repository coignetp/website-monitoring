import DatabaseInterface
from main import StoreApp
from flask import request, g
import json


@StoreApp.route("/get_new_alerts/<int:elapsedMs>", methods=["GET"])
def route_alerts(elapsedMs: int):
    """
    Route used by the client to receive the new alerts for elapsedMs ms.
    """

    # Try to insert the metrics in the database
    alerts = StoreApp.config["DBINTERFACE"].get_new_alerts(elapsedMs)

    return json.dumps(alerts)
