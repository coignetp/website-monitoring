import DatabaseInterface
from createWebsitesInformation import create_websites_information
from process.UpdaterManager import UpdaterManager

import utils
from flask import Flask, g
import argparse

# The flask application. Must be global according to Flask design.
StoreApp = Flask(__name__)

# Routes are imported after the application declaration
from routes.metrics import *
from routes.alerts import *
from routes.stats import *


if __name__ == "__main__":
    # Gets arguments
    parser = argparse.ArgumentParser(
        description="Store metrics, alerts and stats for the website monitoring tool"
    )
    parser.add_argument(
        "-v",
        "--verbose",
        help="Activate the verbose mode of the store",
        action="store_true",
    )
    parser.add_argument(
        "--drop-database",
        help="Drop database at the end of the script",
        action="store_true",
    )
    args = parser.parse_args()
    # Activates (or not) the verbose mode
    utils.ARG_VERBOSE = args.verbose

    # Creates the database interface
    dbInterface = DatabaseInterface.DatabaseInterface(
        utils.get_real_path("../config.json")
    )

    dbInterface.connect()

    # Stores the database interface configuration in the app so it can be used
    # by all the routes
    StoreApp.config.update(DBINTERFACE=dbInterface)

    # Gets the list of websites and starts the updaters
    websites, websitesAvailabilityThreshold = create_websites_information(
        utils.get_real_path("../config.json")
    )
    updaterManager = UpdaterManager(
        dbInterface, websites, websitesAvailabilityThreshold
    )

    StoreApp.run()

    updaterManager.stop()
    if args.drop_database:
        dbInterface.drop_database()
