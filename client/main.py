from Dashboard import Dashboard
import utils

import curses
import argparse

if __name__ == "__main__":
    # Gets arguments
    parser = argparse.ArgumentParser(
        description="Displays the dashboard client for the website monitoring tool"
    )
    parser.add_argument(
        "-v",
        "--verbose",
        help="Activate the verbose mode of the dashboard",
        action="store_true",
    )
    args = parser.parse_args()

    # Activates (or not) the verbose mode
    utils.ARG_VERBOSE = args.verbose

    d = Dashboard(utils.get_real_path("../config.json"))
    curses.wrapper(d.run)
