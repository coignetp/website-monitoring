# Introduction

website-monitoring is a project proposed by Datadog.

![screenshot](screenshot.PNG)

The project has multiple parts:
* Client
* Observer
* Store
* Database

Plus a fake server to test some logic of the project.

# How to use
## Requirements
This project has been developped in Python 3.7, it won't work with Python2. \
It has been tested on Windows (10), Linux (Ubuntu) and Mac.

This project uses curses for the client. On Windows, it can't be installed with pip, so you may follow [this](https://stackoverflow.com/questions/32417379/what-is-needed-for-curses-in-python-3-4-on-windows7)

This project uses InfluxDB, so you may install it or use it in a docker if you have docker with
```
docker run -p 8086:8086 \
      -v influxdb:/var/lib/influxdb \
      influxdb
```
This command comes from: https://docs.docker.com/samples/library/influxdb/#using-this-image 

## Install
Get the repository with:
```
git clone git@gitlab.com:coignetp/website-monitoring.git
```
Go in the folder and install the *requirements.txt*. Be careful, depending on your python installation you may need to use pip instead of pip3
```
cd website-monitoring
pip3 install -r requirements.txt
```

Before starting the whole project, you need to have a *config.json*.
You can use the *config.template.json* with renaming it in *config.json*, or simply
```cp config.template.json config.json```
Note that the username and the password to access the database is in the *config.json*. The template has the default InfluxDB credentials which is username root and password root. Don't forget to change it if you want to use something else.

## Start

Now you can start the 3 parts of the project with the follwing commands:
* For the store
```
python3 store/main.py
```
* Then for the observer
```
python3 observer/main.py
```
* Then for the client
```
python3 client/main.py
```
Notes:
* you may need to use the command python instead of python3 depending on your installation.
* you can call the python files from where you want.
* all the python scripts can be called with a `--verbose` (or `-v`) to enable the verbose mode.
* The store/main.py can be called with a `--drop-database` to drop the database at the end of the script.

## Test
To test the alert logic do a 
```
python3 tests/server_test.py
```
To start the fake server, and add it in the config.json with:
```
{
    "url": "http://127.0.0.1:42042",
    "availability_threshold": 0.8
}
```

Then you can start the project with the commands in Start section.

## config.json
A *config.json* file is like this (comments are #):
```
{
    "global": {
        "poll_interval": 3.0           # interval between 2 poll of the same website in seconds
    },
    "database": {
        "host": "localhost",           # address of the influxdb
        "port": 8086,                  # port to speak with the database
        "database_name": "monitoring", # name of the database (can be created by the store)
        "measures_duration": "3w",     # the information are deleted after "measures_duration". 3 weeks here.
        "measures_replication": 1,     # the number of replication of data
        "username": "root",            # username in the database (here is the default one but it should be changed)
        "password": "root"             # password in the database (here is the default one but it should be changed)
    },
    "websites": [ # list of websites to watch
        {
            "url": "https://www.google.com", # the url of the website to watch
            "availability_threshold": 0.8    # the website is considered up or down if its availability (from 0 to 1) is bigger or less than this value
        },
        {
            "url": "https://www.github.com",
            "availability_threshold": 0.8
        },
        {
            "url": "https://www.datadoghq.com",
            "availability_threshold": 0.8
        }
    ]
}

```

# Architecture
## InfluxDB
InfluxDB is used in this project because it is a time series database.
It stores all the requests previoulsy made, some precomputed statistics and the alerts.

Schema:
* **metric**, result of a request of the observer:
```
{
    "measurement": "metric",
    "tags": {
        "url": "http://www.mywebsite.com",
        status_code": statusCode,
        "request_timestamp": timestamp_in_ms,
    },
    "time": set by InfluxDB,
    "fields": {
        "elapsed_time": time_in_ms
    },
}
```

* **statistics_600s**, **statistics_3600s**, the different statistics for 2 windows (10min and 1h). Has all the status codes received in the last windowLength seconds. *status_codes* is a string because influxdb can't handle an object inside *fields*.
```
{
    "measurement": "statistic_{windowLength}s",
    "tags": {
        "url": "http://www.mywebsite.com"
    },
    "time": set by InfluxDB,
    "fields": {
        "last_average_response_time": time_in_ms,
        "last_maximum_response_time": time_in_ms,
        "last_minimum_response_time": time_in_ms,
        "last_percentile10_response_time": time_in_ms,
        "last_percentile90_response_time": time_in_ms,
        "status_codes": "{
            200: 50,
            500: 12,
        }",
    },
}
```

* **alert**:
```
{
    "measurement": "alert",
    "tags": {
        "url": "http://www.mywebsite.com"
    },
    "time": set by InfluxDB,
    "fields": {
        "availability": 0.98, # From 0 to 1
        "available": True, # Is the website actually up
    },
}

```

## Store
Store is close to the database. It is divided in 3 parts:
* The database interface used to query or write points in influxdb
* routes which can be used by the observer and the client to send or request the metrics, the statistics and the alerts
    * (POST) /metrics
      Takes the list of metrics sent with a JSON and writes it in the database. The JSON sent looks like this:
      ```
      [
        {
            'measurement': 'metric', 
            'tags': {
                'url': 'https://www.google.com', 
                'request_timestamp': 1537590509856,
                'status_code': 200,
            }, 
            'fields': { 
                'elapsed_time': 134
            }
        }
      ]
      ```
    * (GET) /get_last_stat
      Gives the last statistic of a specific windowLength about a specific website. The JSON sent looks like this:
      ```
      {
        'url': 'https://www.google.com',
        'window_length': 600
      }
      ```
    * (GET) /get_new_alerts/<int:elapsedMs>
      Gives all the alert raised in the last elapsedMs milliseconds.
* process which periodically computes the statistics and raises alerts if necessary.
    * Statistics on the 10 last min computed every 10 seconds
    * Statistics on the last hour computed every minute
    * Alerts on the last 2min checked every 5 seconds


The port of the store is 5000.

## Observer
The role of the observer is to send requests to the given websites and send the responses to the store.
Makes a request to all the websites in the config.json file periodically (the interval is set in the config.json as well).
After a request has been made, the observer sends it to the store for it to put it in influxdb and compute statistics & alerts. Then it requests the next website.

## Client
The client displays the statistics and the alerts periodically asked to store.
* Statistics on 10min: refreshed every 10sec
* Statistics on 60min: refreshed every 60sec
* Alerts (on 2min): refreshed around every 5sec

The *Websites* part of the client can handle a very long list of websites, the choices can be scrolled with the arrows.

# How to improve
## Global improvements
* Write a docker-compose.yml to start the database and the scripts automatically (Work in progress not finished in the docker-compose branch).

## Observer improvements
Refactor the metrics sending:
Actually, the websites are requested one by one, and a metric is sent after each request. \
request website A -> send metric to the store -> request website B -> send metric to the store -> .. -> sleep -> back to the begining. \
There are 2 problems here:
* The first one is that if a website is taking a long time to answer, it's blocking the observer from making other request until the answer (or timeout of 2sec). A solution could be to parallelize some requests to be able to get the metrics more quickly
* The second one is that the metrics are sent one by one at the store, even if it can receive a list of metric. It could overload the communication between the observer and the store. A solution could be to put every metrics in a queue and send them all at once to the store every x seconds. 

## Store improvements
* Computing other statistics on the websites, like the statistics over the last 24h

## Client improvements
* For the client to be more graphical, it could be good to display graphics of the last statistics. For example a graph on the average response time of the the last 10min.
* For the moment, the user can scroll on the websites list in WindowWebsites, but not in the alerts list. Only the last ones are displayed. It could be interesting for the user to be able to scroll to have older alerts.