import DatabaseInterface
from main import StoreApp
from flask import request, g
import json


def are_metrics_conform(metrics: list) -> bool:
    """
    Checks if all the required field for an influxdb measure are here
    and if the measurement has the correct name.
    Could eventually check more.
    """
    for metric in metrics:
        if (
            metric.keys() < {"measurement", "tags", "fields"}
            or metric["measurement"] != "metric"
        ):
            return False

    return True


@StoreApp.route("/metrics", methods=["POST"])
def route_metrics():
    """
    Route used by the observer to add metrics. 
    The metrics are sent as a JSON, and this is a list of metric in case the 
    observer wants to send more than one metric. Moreover, influxdb-python 
    needs a list of dict when write_points is called.

    Example of metrics: 
    [
        {
            'measurement': 'metric', 
            'tags': {
                'url': 'https://www.google.com', 
                'request_timestamp': 1537590509856,
                'status_code': 200,
            }, 
            'fields': { 
                'elapsed_time': 134
            }
        }
    ]
    """
    metrics = request.get_json()

    # Check if the metrics can be inserted in the database
    if not are_metrics_conform(metrics):
        badMetrics = f"Bad metrics {metrics}"
        print(badMetrics)
        return badMetrics

    # Try to insert the metrics in the database
    if not StoreApp.config["DBINTERFACE"].write(metrics):
        return f"Metrics {metrics} couldn't be inserted"

    return "Metrics successfully inserted"
