from utils import print_verbose
from windows.WindowWebsites import WindowWebsites
from windows.WindowAlerts import WindowAlerts
from windows.WindowStatistics import WindowStatistics
from fetch.Website import Website
from fetch.UpdaterManager import UpdaterManager
from fetch.getOldAlerts import get_old_alerts

import curses
import json


class Dashboard:
    """
    The main class of the client. It will display the different windows on 
    the screen and update the statistics and alerts.
        - windowWebsites: the part that will display the websites choices
        - windowAlerts: the part that will display the alerts in the right order
        - windowStatistics: the part that will display the statistics about the
          selected website
        - screen: the curses screen to display the dashboard
        - websites: the list of websites with their statistics
        - updaters: used ti call periodically functions to update statistics 
          and alerts
        - colors: map the color id and the name for every color used in this
          dashboard
    """

    def __init__(self, configFileName: str):
        """
        Initializes the Dashboard before calling curses.
            - configFileName: the config.json
        """

        self.windowWebsites = None
        self.windowAlerts = None
        self.windowStatistics = None
        self.screen = None

        # Reads only the websites to have a list of them
        configFile = open(configFileName)
        config = json.load(configFile)
        self.websites = [
            Website(website["url"], website["availability_threshold"])
            for website in config["websites"]
        ]

        self.updaterManager = UpdaterManager(self.websites)

        self.colors = {
            "text": 1,
            "title": 2,
            "bad_news": 3,
            "title2": 4,
            "good_news": 5,
            "screen_background": 6,
            "highlight": 7,
        }

    def prepare_colors(self):
        """
        Prepares all the colors which will be used by the windows
        """
        curses.start_color()
        curses.init_pair(self.colors["text"], curses.COLOR_WHITE, curses.COLOR_BLACK)
        curses.init_pair(self.colors["title"], curses.COLOR_YELLOW, curses.COLOR_BLACK)
        curses.init_pair(self.colors["bad_news"], curses.COLOR_RED, curses.COLOR_BLACK)
        curses.init_pair(self.colors["title2"], curses.COLOR_BLUE, curses.COLOR_BLACK)
        curses.init_pair(
            self.colors["good_news"], curses.COLOR_GREEN, curses.COLOR_BLACK
        )
        curses.init_pair(
            self.colors["screen_background"], curses.COLOR_GREEN, curses.COLOR_BLACK
        )
        curses.init_pair(
            self.colors["highlight"], curses.COLOR_BLACK, curses.COLOR_WHITE
        )

    def prepare_windows(self):
        """
        Creates the windows with the correct size.
        The dashboard is divied in three vertical parts.
        Website | Statistics | Alerts
        """
        screenHeight, screenWidth = self.screen.getmaxyx()

        self.windowWebsites = WindowWebsites(
            self.screen,
            curses.newwin(screenHeight - 1, screenWidth // 3, 0, 0),
            "Websites",
            self.colors,
            self.websites,
        )

        self.windowStatistics = WindowStatistics(
            self.screen,
            curses.newwin(screenHeight - 1, screenWidth // 3, 0, (screenWidth // 3)),
            "Statistics",
            self.colors,
            self.websites[0],
        )

        self.windowAlerts = WindowAlerts(
            self.screen,
            curses.newwin(screenHeight - 1, screenWidth // 3, 0, 2 * screenWidth // 3),
            "Alerts",
            self.colors,
        )

        # For footer display
        self.display_footer()
        self.screen.chgat(screenHeight - 1, 0, screenWidth, curses.A_REVERSE)

        self.windowWebsites.display(self.screen, self.colors)
        self.windowStatistics.display(self.screen, self.colors)
        self.windowAlerts.display(self.screen, self.colors)

    def prepare(self):
        """
        Can't be the constructor __init__ because the dashboard only starts when
        run is called by curses.
        """

        self.prepare_colors()

        self.screen.clear()

        self.prepare_windows()

        self.screen.refresh()

        self.updaterManager.start(self.windowAlerts)

        get_old_alerts(
            "http://127.0.0.1:5000/get_new_alerts",
            self.websites,
            2 * 60 * 60,
            self.windowAlerts,
        )

    def run(self, screen):
        """
        The main loop of the dashboard. This method is called by the curses wrapper.
        """
        self.screen = screen
        self.prepare()

        keyPressed = 0

        while keyPressed != ord("q"):
            self.display_windows()

            # Refresh the screen
            self.screen.refresh()

            # Wait for next input
            keyPressed = screen.getch()

            # Analyze key pressed
            self.react_to_key_pressed(keyPressed)

        self.updaterManager.delete()

    def react_to_key_pressed(self, keyPressed: int):
        """
        Calls the right function according to the key pressed.
        """
        if keyPressed == curses.KEY_DOWN:
            self.windowWebsites.choice_down()
            self.windowStatistics.set_website(
                self.websites[self.windowWebsites.actualChoice]
            )
        elif keyPressed == curses.KEY_UP:
            self.windowWebsites.choice_up()
            self.windowStatistics.set_website(
                self.websites[self.windowWebsites.actualChoice]
            )

    def display_footer(self):
        """
        Displays information in the footer
        """

        self.screen.addstr(
            self.screen.getmaxyx()[0] - 1,
            0,
            "Press Q to quit | Use arrows to navigate | Press R to refresh screen",
            curses.color_pair(self.colors["text"]),
        )

    def display_windows(self):
        """ 
        Displays all the 3 windows
        """
        self.windowWebsites.display(self.screen, self.colors)
        self.windowStatistics.display(self.screen, self.colors)
        self.windowAlerts.display(self.screen, self.colors)
