import os

ARG_VERBOSE = False


def print_verbose(string: str):
    if ARG_VERBOSE:
        print(string)


def get_real_path(path: str) -> str:
    """
    Transforms a relative path to an absolute path.
    Relative paths are relative to current working directory, so it
    might cause some errors if it is not taken into account.
    """
    dirname = os.path.abspath(os.path.dirname(__file__))

    return os.path.join(dirname, path)


def int_to_str_len_n(number: int, stringLength: int) -> str:
    """
    Returns the int converted into a stringLength size string.
    If the number is too big, it is truncated.
    If the number is too small, space char (" ") will be added at the end.
    """

    return (str(number) + (" " * stringLength))[:stringLength]
