import curses
import json
import requests
from utils import int_to_str_len_n


class Website:
    """
    Contains all the information about a specific website to display them.
        - url: the url of this website
        - threshold: the availabilityThreshold. Describes when an alert
            should be raised
        - available: true if the website is currently available
        - statusString: to better display available attribute
        - statistics: dict of all the last statistics according to their windowLength
        - statisticsString: dict of string version of statistics
    """

    def __init__(self, url: str, threshold: float):
        """
        Initiates the website object.
            - url: the url of the website
            - threshold: the availabilityThreshold . Describes when an alert
            should be raised 
        """
        self.url = url
        self.threshold = threshold
        # Without information, a website is considered available
        self.available = True
        self.statusString = "[OK]"

        # All the statistics are initialized with this dict
        defaultStatistic = {
            "time": "1970-01-01T00:00:00Z",
            "last_availability": 0,
            "last_average_response_time": 0,
            "last_maximum_response_time": 0,
            "last_minimum_response_time": 0,
            "last_percentile10_response_time": 0,
            "last_percentile90_response_time": 0,
            "last_status_codes": {"200": 0},
        }

        self.statistics = {
            f"{10 * 60}": defaultStatistic,
            f"{60 * 60}": defaultStatistic,
        }
        self.statisticsString = {
            f"{10 * 60}": self.get_stats_string(str(10 * 60)),
            f"{60 * 60}": self.get_stats_string(str(60 * 60)),
        }

    def display_as_choice(
        self, window, y: int, x: int, colors: dict, highlighted: bool
    ):
        """
        Displays this website on the window parameter at position (y, x) with
        the correct colors as a choice in WindowWebsites.
            - highlighted: True if this choice is currently selected
        """
        # Updates statusString
        self.statusString = "[OK]" if self.available else "[DOWN]"

        # Displays the status of the website
        window.addstr(
            y,
            x,
            self.statusString,
            curses.color_pair(
                colors["good_news"] if self.available else colors["bad_news"]
            ),
        )

        # Displays the url of the website
        window.addstr(
            y,
            x + 7,
            self.url,
            curses.color_pair(colors["highlight"])
            if highlighted
            else curses.color_pair(colors["text"]),
        )

    def display_statistic_title(
        self, window, y: int, windowLength: int, refreshTime: int, colors: dict
    ):
        """
        Displays the title of a statistic at the correct place.
        windowLength and refreshTime are the properties of a statistic in seconds
        """
        window.addstr(
            y, 1, "-" * (window.getmaxyx()[1] - 2), curses.color_pair(colors["title"])
        )

        window.addstr(
            y + 1,
            1,
            f"Window {windowLength}s refreshed every {refreshTime}s",
            curses.color_pair(colors["title"]),
        )

        window.addstr(
            y + 2,
            1,
            "Availability: {:.2f}".format(
                self.statistics[str(windowLength)]["last_availability"] * 100
            )
            + "%",
            curses.color_pair(colors["title"]),
        )

    def display_as_statistics(self, window, colors: dict):
        """
        Displays the statistics of this website on the window given.
        """
        try:
            self.display_as_choice(window, 1, 1, colors, False)

            self.display_statistic_title(window, 2, 60 * 10, 10, colors)
            window.addstr(
                5,
                1,
                self.get_stats_string(str(10 * 60)),
                curses.color_pair(colors["text"]),
            )

            self.display_statistic_title(
                window, window.getmaxyx()[0] // 2 + 1, 60 * 60, 60, colors
            )
            window.addstr(
                window.getmaxyx()[0] // 2 + 4,
                1,
                self.get_stats_string(str(60 * 60)),
                curses.color_pair(colors["text"]),
            )
        except Exception as e:
            print(
                f"Statstics about {self.url} couldn't be displayed. The terminal may be too small"
            )
            print(e)

    def get_stats_string(self, statKey: str) -> str:
        """
        Returns the information of the statistics as a string ready to be
        displayed
        """
        if statKey not in self.statistics.keys():
            print("Wrong statistic asked")
            return ""

        stat = self.statistics[statKey]

        avgTimeSized = int_to_str_len_n(stat["last_average_response_time"], 5)
        minTimeSized = int_to_str_len_n(stat["last_minimum_response_time"], 5)
        maxTimeSized = int_to_str_len_n(stat["last_maximum_response_time"], 5)
        p90TimeSized = int_to_str_len_n(stat["last_percentile90_response_time"], 5)
        p10TimeSized = int_to_str_len_n(stat["last_percentile10_response_time"], 5)

        statsString = f"""Response time (ms):
    +-----+-----+-----+-----+-----+
    | Min | P10 | Avg | P90 | Max |
    +-----+-----+-----+-----+-----+
    |{minTimeSized}|{p10TimeSized}|{avgTimeSized}|{p90TimeSized}|{maxTimeSized}|
    +-----+-----+-----+-----+-----+
    
 Number of status code:\n"""

        # Adds the status code
        for statusCode in stat["last_status_codes"].keys():
            numberOfStatusCode = stat["last_status_codes"][statusCode]
            statsString += f"\t- {statusCode}: {numberOfStatusCode}\n"

        return statsString

    def update_statistics(self, getStatRoute: str, windowLength: int):
        """
        Updates the statistic made on windowLength seconds of this website. The route
        needs a json for the get request. Here is an exemple.
        {
            "url": "https://www.google.com", 
            "window_length": 120
        }

            - getStatRoute: the route to call
            - windowLength: the length in seconds of the statistic window
        """

        try:
            statRequest = requests.get(
                getStatRoute,
                json={"url": self.url, "window_length": windowLength},
                timeout=8,
            )

            statistic = json.loads(statRequest.text)

            # If it is not empty:
            if statistic:
                self.statistics[str(windowLength)] = statistic
        except Exception as e:
            print(
                f"Client can't contact {getStatRoute} with windowLength {windowLength}"
            )
            print(e)

    def set_availability(self, actuallyAvailable: bool, actualAvailability: float):
        """
        Sets self.availability and self.available for this website
        """

        self.available = actuallyAvailable
        self.availability = actualAvailability
